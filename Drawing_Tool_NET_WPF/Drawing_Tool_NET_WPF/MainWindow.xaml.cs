﻿using Drawing_Tool_NET_WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Drawing_Tool_NET_WPF.Helpers;
using Drawing_Tool_NET_WPF.ExtensionMethods;
using Drawing_Tool_NET_WPF.Constants;

namespace Drawing_Tool_NET_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Tab> Tabs { get; set; } = new List<Tab>();
        private Tab SelectedTab => Tabs.GetSelectedTab(tabControl1);
        private bool HasSelectedTab => SelectedTab != null;
        private bool HasSelectedDrawingShape => HasSelectedTab && SelectedTab.HasSelectedDrawingShape;
        private bool HasSelectedShapes => HasSelectedTab && SelectedTab.HasSelectedShapes;


        public MainWindow()
        {
            InitializeComponent();
            ButtonAddTab_Click(null, null);//for testing purposes
        }

        private void ButtonAddTab_Click(object sender, RoutedEventArgs e)
        {
            string tabHeader = $"tab {Tabs.Count + 1}";
            Tabs.AddAndVisualize(new Tab(tabHeader), tabControl1);
        }

        private void SelectDrawingShape_Click(object sender, RoutedEventArgs e)
        {
            if (!HasSelectedTab) { return; }

            SetTabMode_Click(sender, e);

            string shapeTag = (sender as Button)?.Tag.ToString();

            var fill = colorPickerFill.SelectedColor;
            var stroke = colorPickerStroke.SelectedColor;
            var strokeThickness = (double)numericUpDownStrokeThickness.Value;

            var shape = ShapeHelper.CreateShape(shapeTag, fill, stroke, strokeThickness);

            SelectedTab.SelectedDrawingShape = shape;
        }

        private void ColorPickerStroke_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            var strokeColor = e.NewValue;
            if (!HasSelectedTab || strokeColor == null) { return; }

            if (HasSelectedDrawingShape)
            {
                SelectedTab.SelectedDrawingShape.SetStrokeFromColor(strokeColor);
            }

            SelectedTab.SelectedShapes?.ForEach(s => s.SetStrokeFromColor(strokeColor));
        }

        private void ColorPickerFill_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            var fillColor = e.NewValue;
            if (!HasSelectedTab || fillColor == null) { return; }

            if (HasSelectedDrawingShape)
            {
                SelectedTab.SelectedDrawingShape.SetFillFromColor(fillColor);
            }

            SelectedTab.SelectedShapes?.ForEach(s => s.SetFillFromColor(fillColor));
        }

        private void NumericUpDownStrokeThickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (!HasSelectedTab || e.NewValue == null) { return; }

            var strokeThickness = double.Parse(e.NewValue.ToString());
            if (HasSelectedDrawingShape)
            {
                SelectedTab.SelectedDrawingShape.StrokeThickness = strokeThickness;
            }

            SelectedTab.SelectedShapes?.ForEach(s => s.Path.StrokeThickness = strokeThickness);
        }
        private void SetTabMode_Click(object sender, RoutedEventArgs e)
        {
            if (!HasSelectedTab) { return; }

            string buttonTag = (sender as Button)?.Tag.ToString();
            SelectedTab.SetTabMode(buttonTag);
        }

        private void MenuItemRemoveCurrentTab_Click(object sender, RoutedEventArgs e)
        {
            if (!HasSelectedTab) { return; }

            SelectedTab.RemoveCurrentTab(tabControl1, Tabs);
        }

        private void ContextMenuTabControlShapes_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            menuItemRemoveCurrentTab.IsEnabled = HasSelectedTab;

            menuItemRotateSelectedShapes.IsEnabled = HasSelectedShapes;
            menuItemRemoveSelectedShapes.IsEnabled = HasSelectedShapes;
        }

        private void MenuItemRotateSelectedShapes_Click(object sender, RoutedEventArgs e)
        {
            SelectedTab.RotateSelectedShapes(90, true);
        }

        private void ButtonScaleDown_Click(object sender, RoutedEventArgs e)
        {
            if (!HasSelectedShapes) { return; }

            SelectedTab.SelectedShapes.ForEach(s => s.Scale(-ShapeConstants.SCALE_COEF));
        }

        private void ButtonScaleUp_Click(object sender, RoutedEventArgs e)
        {
            if (!HasSelectedShapes) { return; }

            SelectedTab.SelectedShapes.ForEach(s => s.Scale(ShapeConstants.SCALE_COEF));
        }

        private void MenuItemRemoveSelectedShapes_Click(object sender, RoutedEventArgs e)
        {
            SelectedTab.RemoveSelectedShapes();
        }

        private async void ButtonSaveAsImage_Click(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.InvokeAsync(() =>
            {
                IOHelper.SaveAsImage(SelectedTab);
            });
        }

        private void ButtonSaveProject_Click(object sender, RoutedEventArgs e)
        {
            IOHelper.SaveProject(Tabs);
        }

        private void ButtonLoadProject_Click(object sender, RoutedEventArgs e)
        {
            IOHelper.LoadProject(out List<Tab> tabs);

            if (tabs == null) { return; }

            Tabs = tabs;
            Tabs.LoadTabs(tabControl1);
        }
    }
}
