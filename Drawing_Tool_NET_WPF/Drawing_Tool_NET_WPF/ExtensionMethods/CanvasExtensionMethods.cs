﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Drawing_Tool_NET_WPF.Interfaces;

namespace Drawing_Tool_NET_WPF.ExtensionMethods
{
    public static class CanvasExtensionMethods
    {
        public static void DrawShape(this Canvas canvas, IDrawable shape, bool clearCanvas = false)
        {
            if (clearCanvas)
            {
                canvas.Children.Clear();
            }

            canvas.Children.Add(shape.Path);
        }

        public static void DrawShapes(this Canvas canvas, List<IDrawable> shapes)
        {
            canvas.Children.Clear();

            for (int i = 0; i < shapes.Count; i++)
            {
                canvas.Children.Add(shapes[i].Path);
            }
        }
    }
}
