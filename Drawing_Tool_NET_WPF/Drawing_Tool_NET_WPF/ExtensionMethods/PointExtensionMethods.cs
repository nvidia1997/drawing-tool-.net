﻿using System;
using System.Linq;
using System.Text;
using System.Windows;

namespace Drawing_Tool_NET_WPF.ExtensionMethods
{
    public static class PointExtensionMethods
    {
        public static Point CenterBetween(this Point p1, Point p2)
        {
            var p1X = p1.X;
            var p1Y = p1.Y;

            var p2X = p2.X;
            var p2Y = p2.Y;

            double centerX = p1X > p2X ? p2X + (p1X - p2X) / 2 : p1X + (p2X - p1X) / 2;
            double centerY = p1Y > p2Y ? p2Y + (p1Y - p2Y) / 2 : p1Y + (p2Y - p1Y) / 2;

            return new Point(centerX, centerY);
        }

        public static Point DistanceTo(this Point p1, Point p2)
        {
            Vector rawDistance = Point.Subtract(p1, p2);
            double distanceX = Math.Abs(rawDistance.X);
            double distanceY = Math.Abs(rawDistance.Y);
            return new Point(distanceX, distanceY);
        }
    }
}
