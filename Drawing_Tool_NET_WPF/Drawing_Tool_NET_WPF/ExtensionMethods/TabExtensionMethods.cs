﻿using System;
using System.Text;
using Drawing_Tool_NET_WPF.Constants;
using Drawing_Tool_NET_WPF.Models;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;

namespace Drawing_Tool_NET_WPF.ExtensionMethods
{
    public static class TabExtensionMethods
    {
        public static Tab GetSelectedTab(this List<Tab> tabs, TabControl tabControl)
        {
            return tabs.FirstOrDefault(t => t.TabItem == tabControl.SelectedItem);
        }

        public static void SetTabMode(this Tab tab, string buttonTag)
        {
            switch (buttonTag.ToLower().Trim())
            {
                case ShapeConstants.SELECTOR:
                    tab.TabMode = TabModes.Selector;
                    return;
                case ButtonConstants.MOVE:
                    tab.TabMode = TabModes.Move;
                    return;
                case ButtonConstants.ROTATE:
                    tab.TabMode = TabModes.Rotate;
                    return;
                default:
                    tab.TabMode = TabModes.Draw;
                    return;
            }
        }

        public static void SetSelectedShapes(this Tab tab)
        {
            var selectorShapeGeometry = tab.SelectorShape.Path.Data;

            var selectedShapes = tab.Shapes.Where(shape =>
            {
                IntersectionDetail intersectionType = selectorShapeGeometry.FillContainsWithDetail(shape.Path.Data);
                bool isContained = intersectionType != IntersectionDetail.Empty && intersectionType != IntersectionDetail.NotCalculated;
                return isContained;
            });
            tab.SelectedShapes = selectedShapes.ToList();
        }

        public static void RemoveSelectorShape(this Tab tab)
        {
            tab.SelectedDrawingShape = null;
            tab.SelectorShape = null;

            tab.Canvas.DrawShapes(tab.Shapes);
        }

        public static void RotateSelectedShapes(this Tab tab, int delta, bool useDeltaAsDegree = false)
        {
            if (!tab.HasSelectedShapes) { return; }

            int degrees;
            if (useDeltaAsDegree)
            {
                degrees = delta;
            }
            else
            {
                degrees = delta < 0 ? -1 : 1;
            }

            for (int i = 0; i < tab.SelectedShapes.Count; i++)
            {
                tab.SelectedShapes[i].Rotate(degrees);
            }
        }

        public static void RemoveSelectedShapes(this Tab tab)
        {
            if (!tab.HasSelectedShapes) { return; }

            tab.Shapes = tab.Shapes.Where(s => !tab.SelectedShapes.Contains(s)).ToList();
            tab.SelectedShapes = null;
            tab.Canvas.DrawShapes(tab.Shapes);
        }

        public static void RemoveCurrentTab(this Tab selectedTab, TabControl tabControl, List<Tab> tabs)
        {
            tabs.Remove(selectedTab);
            tabControl.Items.Clear();

            if (tabs.Count == 0) { return; }

            for (int i = 0; i < tabs.Count; i++)
            {
                var currentTab = tabs[i];
                tabControl.Items.Add(currentTab.TabItem);
            }
            tabControl.SelectedIndex = 0;
        }

        public static void AddAndVisualize(this List<Tab> tabs, Tab tabToAdd, TabControl tabControl)
        {
            tabs.Add(tabToAdd);
            tabControl.Items.Add(tabToAdd.TabItem);
            tabControl.SelectedIndex = tabControl.Items.IndexOf(tabToAdd.TabItem);
        }

        public static void LoadTabs(this List<Tab> tabs, TabControl tabControl)
        {
            tabControl.Items.Clear();

            if (tabs.Count == 0) { return; }

            for (int i = 0; i < tabs.Count; i++)
            {
                var currentTab = tabs[i];
                tabControl.Items.Add(currentTab.TabItem);
                currentTab.Canvas.DrawShapes(currentTab.Shapes);
            }
            tabControl.SelectedIndex = 0; // select the first tab
        }
    }
}
