﻿using Drawing_Tool_NET_WPF.Constants;
using Drawing_Tool_NET_WPF.Interfaces;
using Drawing_Tool_NET_WPF.Models.Shapes;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Drawing_Tool_NET_WPF.Constants.SerializationConstants;

namespace Drawing_Tool_NET_WPF.Converter
{
    public class ShapeConverter
    {
        public static List<IDrawable> Convert(List<object> rawShapes)
        {
            List<IDrawable> convertedShapes = new List<IDrawable>();
            foreach (JObject rawShape in rawShapes)
            {
                IDrawable tempShape;
                string jsonShape = rawShape.ToString();
                var shapeTag = ((string)rawShape.Property(ShapeSerializationConstants.TAG).Value).ToLower().Trim();
                switch (shapeTag)
                {
                    case ShapeConstants.LINE:
                        tempShape = JsonConvert.DeserializeObject<LineShape>(jsonShape);
                        break;
                    case ShapeConstants.RECTANGLE:
                        tempShape = JsonConvert.DeserializeObject<RectangleShape>(jsonShape);
                        break;
                    case ShapeConstants.ELLIPSE:
                        tempShape = JsonConvert.DeserializeObject<EllipseShape>(jsonShape);
                        break;
                    case ShapeConstants.RECTANGLE_DIAGONAL:
                        tempShape = JsonConvert.DeserializeObject<RectangleDiagonalShape>(jsonShape);
                        break;
                    default:
                        throw new NotImplementedException(ShapeConstants.UNSUPPORTED_SHAPE + shapeTag);
                }

                convertedShapes.Add(tempShape);
            }
            return convertedShapes;
        }
    }
}
