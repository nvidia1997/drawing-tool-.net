﻿using Drawing_Tool_NET_WPF.Constants;
using Drawing_Tool_NET_WPF.Interfaces;
using Drawing_Tool_NET_WPF.Models.Shapes;
using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Drawing_Tool_NET_WPF.Helpers
{
    public class ShapeHelper
    {
        public static IDrawable CreateShape(string shapeTag, Color? fill, Color? stroke, double strokeThickness)
        {
            IDrawable shape;
            string tag = shapeTag.ToLower().Trim();
            switch (tag)
            {
                case ShapeConstants.LINE:
                    shape = new LineShape();
                    break;
                case ShapeConstants.RECTANGLE:
                    shape = new RectangleShape();
                    break;
                case ShapeConstants.ELLIPSE:
                    shape = new EllipseShape();
                    break;
                case ShapeConstants.RECTANGLE_DIAGONAL:
                    shape = new RectangleDiagonalShape();
                    break;
                case ShapeConstants.SELECTOR:
                    shape = new SelectorShape();
                    break;
                default:
                    throw new NotImplementedException(ShapeConstants.UNSUPPORTED_SHAPE + tag);
            }

            if (shapeTag != ShapeConstants.SELECTOR)
            {
                if (stroke.HasValue)
                {
                    shape.SetStrokeFromColor(stroke.Value);
                }
                if (fill.HasValue)
                {
                    shape.SetFillFromColor(fill.Value);
                }

                shape.StrokeThickness = strokeThickness;
            }

            return shape;
        }
    }
}
