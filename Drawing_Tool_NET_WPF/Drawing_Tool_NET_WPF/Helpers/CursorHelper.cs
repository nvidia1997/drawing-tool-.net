﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Drawing_Tool_NET_WPF.Helpers
{
   public class CursorHelper
    {
        public static Point GetCursorPoint(Canvas canvas)
        {
            Point cursorPosition = Mouse.GetPosition(canvas);
            return cursorPosition;
        }
    }
}
