﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Microsoft.Win32;
using Drawing_Tool_NET_WPF.Models;

namespace Drawing_Tool_NET_WPF.Helpers
{
    public static class IOHelper
    {
        const string jsonFilter = "json files (*.json)|*.json";

        public static void SaveAsImage(Tab selectedTab)
        {
            var saveFileDialog = CreateSaveFileDialog("png (*.png)|*.png|jpg (*.jpg)|*.jpg|gif (*.gif)|*.gif");
            if (saveFileDialog == null) { return; }

            string filename = saveFileDialog.FileName;
            string fileExtension = Path.GetExtension(filename).ToLower();

            BitmapEncoder encoder;
            if (fileExtension == ".gif")
                encoder = new GifBitmapEncoder();
            else if (fileExtension == ".png")
                encoder = new PngBitmapEncoder();
            else if (fileExtension == ".jpg")
                encoder = new JpegBitmapEncoder();
            else
                return;


            var canvas = selectedTab.Canvas;

            int Height = (int)canvas.ActualHeight;
            int Width = (int)canvas.ActualWidth;
            RenderTargetBitmap bmp = new RenderTargetBitmap(Width, Height, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(canvas);

            encoder.Frames.Add(BitmapFrame.Create(bmp));


            try
            {
                using (Stream stream = File.Create(filename))
                {
                    encoder.Save(stream);
                }
            }
            catch (Exception e)
            {
                throw e;
            }


        }
        public static void SaveProject(List<Tab> tabs)
        {
            var saveFileDialog = CreateSaveFileDialog(jsonFilter);
            if (saveFileDialog == null) { return; }

            try
            {
                var jsonSerializerSettings = new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Error,
                    Formatting = Formatting.Indented,
                    TypeNameHandling = TypeNameHandling.Auto,
                };
                string json = JsonConvert.SerializeObject(tabs, jsonSerializerSettings);
                File.WriteAllText(saveFileDialog.FileName, json);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void LoadProject(out List<Tab> tabs)
        {
            var openFileDialog = new OpenFileDialog()
            {
                Filter = jsonFilter,
            };
            if (openFileDialog.ShowDialog() != true)
            {
                tabs = null;
                return;
            }

            try
            {
                var jsonSerializerSettings = new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Error,
                    TypeNameHandling = TypeNameHandling.Auto,
                };
                var json = File.ReadAllText(openFileDialog.FileName);
                tabs = JsonConvert.DeserializeObject<List<Tab>>(json);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter">example value "txt files (*.txt)|*.txt|All files (*.*)|*.*"</param>
        /// <param name="filterIndex"></param>
        /// <param name="restoreDirectory"></param>
        /// <returns></returns>
        private static SaveFileDialog CreateSaveFileDialog(string filter, int filterIndex = 1, bool restoreDirectory = true)
        {
            var saveFileDialog = new SaveFileDialog()
            {
                Filter = filter,
                FilterIndex = filterIndex,
                RestoreDirectory = restoreDirectory,
                FileName = "save",
            };
            bool? isSaveClicked = saveFileDialog.ShowDialog();
            return isSaveClicked == true ? saveFileDialog : null;
        }
    }
}
