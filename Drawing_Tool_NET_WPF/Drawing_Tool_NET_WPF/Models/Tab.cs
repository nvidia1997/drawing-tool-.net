﻿using Drawing_Tool_NET_WPF.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using Drawing_Tool_NET_WPF.Helpers;
using Drawing_Tool_NET_WPF.ExtensionMethods;
using System.Windows.Input;
using System.Runtime.Serialization;
using Drawing_Tool_NET_WPF.Models.Shapes;
using Drawing_Tool_NET_WPF.Converter;
using Drawing_Tool_NET_WPF.Constants.SerializationConstants;

namespace Drawing_Tool_NET_WPF.Models
{
    public enum TabModes
    {
        Draw,
        Selector,
        Move,
        Rotate,
    }

    [Serializable]
    public class Tab : ISerializable
    {
        public bool IsMouseLeftButtonDown { get; private set; }

        public Canvas Canvas { get; private set; }
        public TabItem TabItem { get; private set; }

        private TabModes tabMode;
        public TabModes TabMode
        {
            get => tabMode;
            set
            {
                tabMode = value;
                TabMode_Changed();
            }
        }

        private List<IDrawable> shapes;
        public List<IDrawable> Shapes
        {
            get
            {
                if (shapes == null)
                {
                    this.shapes = new List<IDrawable>();
                }

                return shapes;
            }
            set
            {
                this.shapes = value;
            }
        }
        public IDrawable SelectedDrawingShape { get; set; }
        public bool HasSelectedDrawingShape => SelectedDrawingShape != null;

        public List<IDrawable> SelectedShapes { get; set; }
        public bool HasSelectedShapes => SelectedShapes != null && SelectedShapes.Count > 0;

        public IDrawable SelectorShape { get; set; }

        public string Header { get; set; }

        public Tab(string tabHeader)
        {
            Canvas = new Canvas()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Background = Brushes.White, // The background setting is important hack because it enables mouse events capturing !
            };
            Canvas.MouseLeftButtonDown += Canvas_MouseLeftButtonDown;
            Canvas.MouseLeftButtonUp += Canvas_MouseLeftButtonUp;
            Canvas.MouseMove += Canvas_MouseMove;
            Canvas.PreviewMouseMove += Canvas_PreviewMouseMove;
            Canvas.MouseLeave += Canvas_MouseLeave;
            Canvas.MouseWheel += Canvas_MouseWheel;

            this.Header = tabHeader;
            TabItem = new TabItem()
            {
                Header = this.Header,
                Content = Canvas,
            };

            Canvas.SetLeft(TabItem, 0);
            Canvas.SetTop(TabItem, 0);
        }

        public Tab(SerializationInfo info, StreamingContext context)
            : this(
               tabHeader: info.GetString(TabSerializationConstants.HEADER)
                  )
        {
            var rawShapes = (List<object>)info.GetValue(TabSerializationConstants.SHAPES, typeof(List<object>));
            this.Shapes = ShapeConverter.Convert(rawShapes);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(TabSerializationConstants.HEADER, Header);
            info.AddValue(TabSerializationConstants.SHAPES, Shapes);
        }

        private Point previousMousePosition;
        private void Canvas_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (TabMode == TabModes.Move && HasSelectedShapes)
            {
                var cursorPosition = CursorHelper.GetCursorPoint(Canvas);
                if (IsMouseLeftButtonDown)
                {
                    var delta = Point.Subtract(cursorPosition, previousMousePosition);
                    SelectedShapes.ForEach(shape => shape.Move(delta));
                }
                previousMousePosition = cursorPosition;
            }

        }

        private void Canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (TabMode == TabModes.Rotate)
            {
                this.RotateSelectedShapes(e.Delta);
            }
        }

        private void Canvas_MouseLeave(object sender, MouseEventArgs e)
        {
            if (HasSelectedDrawingShape && TabMode == TabModes.Selector)
            {
                this.RemoveSelectorShape();
            }
        }

        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (!IsMouseLeftButtonDown || !HasSelectedDrawingShape) { return; }

            if (TabMode == TabModes.Draw || TabMode == TabModes.Selector)
            {
                var currentPoint = CursorHelper.GetCursorPoint(Canvas);
                SelectedDrawingShape.SetEnd(currentPoint);
                Canvas.DrawShapes(Shapes);
                if (TabMode == TabModes.Selector)
                {
                    Canvas.DrawShape(SelectorShape);
                }
            }
        }

        private void Canvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            IsMouseLeftButtonDown = false;
            if (!HasSelectedDrawingShape) { return; }


            if (TabMode == TabModes.Selector)
            {
                this.SetSelectedShapes();
                this.RemoveSelectorShape();
            }
            if (TabMode == TabModes.Draw)
            {
                SelectedDrawingShape = SelectedDrawingShape.Copy();
            }
        }

        private void Canvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IsMouseLeftButtonDown = true;

            if ((TabMode == TabModes.Draw || TabMode == TabModes.Selector) && HasSelectedDrawingShape)
            {
                var currentPoint = CursorHelper.GetCursorPoint(Canvas);
                SelectedDrawingShape.SetStart(currentPoint);
                if (TabMode == TabModes.Draw)
                {
                    Shapes.Add(SelectedDrawingShape);
                }
                else
                {
                    SelectorShape = SelectedDrawingShape;
                }
            }
        }

        private void TabMode_Changed()
        {
            switch (TabMode)
            {
                case TabModes.Draw:
                    this.Canvas.Cursor = Cursors.Pen;
                    break;
                case TabModes.Selector:
                    this.Canvas.Cursor = Cursors.Cross;
                    break;
                case TabModes.Move:
                    this.Canvas.Cursor = this.HasSelectedShapes ? Cursors.SizeAll : Cursors.Arrow;
                    break;
                case TabModes.Rotate:
                    this.Canvas.Cursor = this.HasSelectedShapes ? Cursors.AppStarting : Cursors.Arrow;
                    break;
                default:
                    this.Canvas.Cursor = Cursors.Arrow;
                    break;
            }
        }
    }
}
