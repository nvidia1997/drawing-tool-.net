﻿using Drawing_Tool_NET_WPF.Constants;
using Drawing_Tool_NET_WPF.Interfaces;
using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Drawing_Tool_NET_WPF.ExtensionMethods;
using System.Runtime.Serialization;

namespace Drawing_Tool_NET_WPF.Models.Shapes
{
    [Serializable]
    public class EllipseShape : ShapeBase, IDrawable
    {
        private EllipseGeometry elipseGeometry;

        public EllipseShape() : base()
        {
            this.SetGeometry();
            this.Tag = ShapeConstants.ELLIPSE;
        }

        public EllipseShape(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            this.SetGeometry();
            this.SetStart(this.StartPoint);
            this.SetEnd(this.EndPoint);
        }

        protected override void SetGeometry()
        {
            elipseGeometry = new EllipseGeometry()
            {
                Transform = Transforms
            };
            this.Geometry = elipseGeometry;
        }

        public override void SetEnd(Point endPoint)
        {
            base.SetEnd(endPoint);

            var radius = StartPoint.DistanceTo(endPoint);
            elipseGeometry.RadiusX = radius.X;
            elipseGeometry.RadiusY = radius.Y;
            elipseGeometry.Center = this.Center;
        }

        public override void SetStart(Point startPoint)
        {
            base.SetStart(startPoint);
        }
    }
}
