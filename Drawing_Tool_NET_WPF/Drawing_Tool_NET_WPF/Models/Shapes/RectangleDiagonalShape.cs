﻿using Drawing_Tool_NET_WPF.Constants;
using Drawing_Tool_NET_WPF.Interfaces;
using Drawing_Tool_NET_WPF.ExtensionMethods;
using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Runtime.Serialization;

// Complex shape test
namespace Drawing_Tool_NET_WPF.Models.Shapes
{
    [Serializable]
    public class RectangleDiagonalShape : ShapeBase, IDrawable
    {
        private RectangleGeometry rectangleGeometry;
        private LineGeometry lineGeometry;

        public RectangleDiagonalShape() : base()
        {
            this.SetGeometry();
            this.Tag = ShapeConstants.RectangleDiagonal;
        }

        public RectangleDiagonalShape(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            this.SetGeometry();
            this.SetStart(this.StartPoint);
            this.SetEnd(this.EndPoint);
        }

        protected override void SetGeometry()
        {
            rectangleGeometry = new RectangleGeometry()
            {
                Rect = new Rect(),
                Transform = Transforms,
            };

            lineGeometry = new LineGeometry()
            {
                Transform = Transforms,
            };
            var geometryGroup = new GeometryGroup();
            geometryGroup.Children.Add(rectangleGeometry);
            geometryGroup.Children.Add(lineGeometry);
            this.Geometry = geometryGroup;
        }

        public override void SetEnd(Point endPoint)
        {
            if (endPoint.X < StartPoint.X || endPoint.Y < StartPoint.Y) { return; }

            base.SetEnd(endPoint);

            Rect rect = rectangleGeometry.Rect;
            rect.Size = (Size)StartPoint.DistanceTo(endPoint);
            rectangleGeometry.Rect = rect;

            lineGeometry.StartPoint = new Point(StartPoint.X, StartPoint.Y + rect.Height);
            lineGeometry.EndPoint = new Point(endPoint.X, endPoint.Y - rect.Height);
        }

        public override void SetStart(Point startPoint)
        {
            base.SetStart(startPoint);

            var rect = rectangleGeometry.Rect;
            rect.Location = startPoint;
            rectangleGeometry.Rect = rect;

            lineGeometry.StartPoint = new Point(startPoint.X, startPoint.Y + rect.Height);
        }
    }
}
