﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Drawing_Tool_NET_WPF.Constants.SerializationConstants;
using Drawing_Tool_NET_WPF.ExtensionMethods;
using Drawing_Tool_NET_WPF.Helpers;
using Drawing_Tool_NET_WPF.Interfaces;

namespace Drawing_Tool_NET_WPF.Models.Shapes
{
    [Serializable]
    public abstract class ShapeBase : IDrawable, ISerializable
    {
        public Path Path { get; set; }
        protected Geometry Geometry { get => Path.Data; set => Path.Data = value; }

        private string tag;
        public string Tag
        {
            get => tag;
            set
            {
                Path.Tag = value;
                this.tag = value;
            }
        }

        public Brush Stroke
        {
            get
            {
                return this.Path.Stroke;
            }
            set
            {
                this.Path.Stroke = value;
            }
        }
        public double StrokeThickness
        {
            get
            {
                return this.Path.StrokeThickness;
            }
            set
            {
                this.Path.StrokeThickness = value;
            }
        }
        public Brush Fill
        {
            get
            {
                return this.Path.Fill;
            }
            set
            {
                this.Path.Fill = value;
            }
        }

        //start cordinates
        protected Point StartPoint { get; private set; }
        // 
        //end cordinates
        protected Point EndPoint { get; private set; }
        //

        protected double RotationAngle { get; set; } = 0;
        protected double ScaleCoef { get; set; } = 1;
        protected Point Translation { get; set; }

        protected RotateTransform RotateTransform { get; private set; }
        protected ScaleTransform ScaleTransform { get; private set; }
        protected TranslateTransform TranslateTransform { get; private set; }
        protected TransformGroup Transforms { get; private set; }

        protected virtual Point Center => this.StartPoint.CenterBetween(this.EndPoint);

        public ShapeBase()
        {
            this.Path = new Path();

            this.RotateTransform = new RotateTransform();
            this.ScaleTransform = new ScaleTransform();
            this.TranslateTransform = new TranslateTransform();

            Transforms = new TransformGroup();
            Transforms.Children.Add(RotateTransform);
            Transforms.Children.Add(ScaleTransform);
            Transforms.Children.Add(TranslateTransform);
        }

        // This special constructor is used to deserialize values.
        public ShapeBase(SerializationInfo info, StreamingContext context) : this()
        {
            this.Tag = info.GetString(ShapeSerializationConstants.TAG);

            this.StartPoint = (Point)info.GetValue(ShapeSerializationConstants.START_POINT, StartPoint.GetType());
            this.EndPoint = (Point)info.GetValue(ShapeSerializationConstants.END_POINT, EndPoint.GetType());

            this.RotationAngle = info.GetDouble(ShapeSerializationConstants.ROTATION_ANGLE);
            this.SetRotation();

            this.ScaleCoef = info.GetDouble(ShapeSerializationConstants.SCALE_COEF);
            this.SetScale();

            this.Translation = (Point)info.GetValue(ShapeSerializationConstants.TRANSLATION, Translation.GetType());
            TranslateTransform.X = this.Translation.X;
            TranslateTransform.Y = this.Translation.Y;


            var brushConverter = new BrushConverter();
            var fill = info.GetString(ShapeSerializationConstants.FILL);
            var stroke = info.GetString(ShapeSerializationConstants.STROKE);
            this.Fill = (Brush)brushConverter.ConvertFromString(fill);
            this.Stroke = (Brush)brushConverter.ConvertFromString(stroke);
            this.StrokeThickness = info.GetDouble(ShapeSerializationConstants.STROKE_THICKNESS);
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(ShapeSerializationConstants.TAG, Tag);

            info.AddValue(ShapeSerializationConstants.START_POINT, StartPoint);
            info.AddValue(ShapeSerializationConstants.END_POINT, EndPoint);

            info.AddValue(ShapeSerializationConstants.ROTATION_ANGLE, RotationAngle);
            info.AddValue(ShapeSerializationConstants.SCALE_COEF, ScaleCoef);
            info.AddValue(ShapeSerializationConstants.TRANSLATION, Translation);

            info.AddValue(ShapeSerializationConstants.STROKE_THICKNESS, StrokeThickness);


            info.AddValue(ShapeSerializationConstants.FILL, this.Fill.ToString());
            info.AddValue(ShapeSerializationConstants.STROKE, this.Stroke.ToString());
        }

        public virtual void SetStart(Point startPoint)
        {
            this.StartPoint = startPoint;
        }

        public virtual void SetEnd(Point endPoint)
        {
            this.EndPoint = endPoint;
        }

        public virtual void SetStrokeFromColor(Color? color)
        {
            this.Stroke = new SolidColorBrush((Color)color);
        }

        public virtual void SetFillFromColor(Color? color)
        {
            this.Fill = new SolidColorBrush((Color)color);
        }

        public virtual IDrawable Copy()
        {
            var shapeTag = (string)this.Path.Tag;
            var fill = ((SolidColorBrush)this.Path.Fill).Color;
            var stroke = ((SolidColorBrush)this.Path.Stroke).Color;
            var strokeThickness = this.Path.StrokeThickness;

            return ShapeHelper.CreateShape(shapeTag, fill, stroke, strokeThickness);
        }

        public virtual void Rotate(double degrees)
        {
            this.RotationAngle += degrees;

            this.SetRotation();
        }

        private void SetRotation()
        {
            this.RotateTransform.Angle = this.RotationAngle;
            this.RotateTransform.CenterX = this.Center.X;
            this.RotateTransform.CenterY = this.Center.Y;
        }

        public virtual void Move(Vector delta)
        {
            TranslateTransform.X += delta.X;
            TranslateTransform.Y += delta.Y;

            this.Translation = new Point(TranslateTransform.X, TranslateTransform.Y);
        }

        public virtual void Scale(double scaleCoef)
        {
            this.ScaleCoef += scaleCoef;

            this.SetScale();
        }

        private void SetScale()
        {
            this.ScaleTransform.ScaleX = this.ScaleCoef;
            this.ScaleTransform.ScaleY = this.ScaleCoef;
        }

        protected abstract void SetGeometry();
    }
}
