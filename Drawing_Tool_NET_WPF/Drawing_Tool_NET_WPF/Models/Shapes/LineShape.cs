﻿using Drawing_Tool_NET_WPF.Constants;
using Drawing_Tool_NET_WPF.Interfaces;
using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Drawing_Tool_NET_WPF.Models.Shapes
{
    [Serializable]
    public class LineShape : ShapeBase, IDrawable, ISerializable
    {
        private LineGeometry lineGeometry;

        public LineShape()
        {
            this.SetGeometry();
            this.Tag = ShapeConstants.LINE;
        }

        public LineShape(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            this.SetGeometry();
            this.SetStart(this.StartPoint);
            this.SetEnd(this.EndPoint);
        }

        protected override void SetGeometry()
        {
            lineGeometry = new LineGeometry()
            {
                Transform = Transforms,
            };
            this.Geometry = lineGeometry;
        }

        public override void SetEnd(Point endPoint)
        {
            base.SetEnd(endPoint);

            lineGeometry.EndPoint = endPoint;
        }

        public override void SetStart(Point startPoint)
        {
            base.SetStart(startPoint);

            lineGeometry.StartPoint = startPoint;
        }
    }
}
