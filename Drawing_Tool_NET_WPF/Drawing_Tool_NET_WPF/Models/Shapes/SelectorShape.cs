﻿using Drawing_Tool_NET_WPF.Constants;
using Drawing_Tool_NET_WPF.Interfaces;
using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Drawing_Tool_NET_WPF.Models.Shapes
{
    [Serializable]
    public class SelectorShape : ShapeBase, IDrawable
    {
        private RectangleGeometry rectangleGeometry;
        public SelectorShape() : base()
        {
            this.SetGeometry();

            var brushConverter = new BrushConverter();
            var FillBrush = (SolidColorBrush)brushConverter.ConvertFromString("#77007196");
            this.Path.Fill = FillBrush;
            this.Stroke = Brushes.AliceBlue;
            this.StrokeThickness = 2;
            this.Tag = ShapeConstants.SELECTOR;
        }

        public SelectorShape(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            this.SetGeometry();
            this.SetStart(this.StartPoint);
            this.SetEnd(this.EndPoint);
        }

        protected override void SetGeometry()
        {
            rectangleGeometry = new RectangleGeometry()
            {
                Rect = new Rect(),
            };
            this.Geometry = rectangleGeometry;
        }

        public override void SetEnd(Point endPoint)
        {
            base.SetEnd(endPoint);

            var rect = rectangleGeometry.Rect;
            var distance = Point.Subtract(StartPoint, endPoint);
            var width = Math.Abs(distance.X);
            var height = Math.Abs(distance.Y);
            rect.Size = new Size(width, height);
            rectangleGeometry.Rect = rect;
        }

        public override void SetStart(Point startPoint)
        {
            base.SetStart(startPoint);

            var rect = rectangleGeometry.Rect;
            rect.Location = startPoint;
            rectangleGeometry.Rect = rect;
        }
    }
}
