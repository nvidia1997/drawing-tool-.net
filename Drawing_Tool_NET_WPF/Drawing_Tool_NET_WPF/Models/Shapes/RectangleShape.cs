﻿using Drawing_Tool_NET_WPF.Constants;
using Drawing_Tool_NET_WPF.ExtensionMethods;
using Drawing_Tool_NET_WPF.Interfaces;
using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Drawing_Tool_NET_WPF.Models.Shapes
{
    [Serializable]
    public class RectangleShape : ShapeBase, IDrawable
    {
        private RectangleGeometry rectangleGeometry;

        public RectangleShape() : base()
        {
            this.SetGeometry();
            this.Tag = ShapeConstants.RECTANGLE;
        }

        public RectangleShape(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            this.SetGeometry();
            this.SetStart(this.StartPoint);
            this.SetEnd(this.EndPoint);
        }

        protected override void SetGeometry()
        {
            rectangleGeometry = new RectangleGeometry()
            {
                Rect = new Rect(),
                Transform = Transforms,
            };
            this.Geometry = rectangleGeometry;
        }

        public override void SetEnd(Point endPoint)
        {
            if (endPoint.X < StartPoint.X || endPoint.Y < StartPoint.Y) { return; }

            base.SetEnd(endPoint);


            Rect rect = rectangleGeometry.Rect;
            rect.Size = (Size)StartPoint.DistanceTo(endPoint);
            rectangleGeometry.Rect = rect;
        }

        public override void SetStart(Point startPoint)
        {
            base.SetStart(startPoint);

            var rect = rectangleGeometry.Rect;
            rect.Location = startPoint;
            rectangleGeometry.Rect = rect;
        }
    }
}
