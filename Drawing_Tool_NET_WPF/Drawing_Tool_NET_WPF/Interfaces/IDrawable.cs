﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;


namespace Drawing_Tool_NET_WPF.Interfaces
{
    public interface IDrawable
    {
        Path Path { get; set; }
        Brush Stroke { get; set; }
        double StrokeThickness { get; set; }
        Brush Fill { get; set; }
        void SetStrokeFromColor(Color? color);
        void SetFillFromColor(Color? color);
        void SetStart(Point startPoint);
        void SetEnd(Point endPoint);
        IDrawable Copy();
        void Rotate(double degrees);
        void Move(Vector delta);
        void Scale(double scaleCoef);
    }
}
