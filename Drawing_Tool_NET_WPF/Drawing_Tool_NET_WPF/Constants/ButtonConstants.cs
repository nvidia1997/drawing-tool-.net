﻿
namespace Drawing_Tool_NET_WPF.Constants
{
    public class ButtonConstants
    {
        // created them as props because data-binding requires it
        public const string MOVE = "move";
        public static string Move => MOVE;

        public const string ROTATE = "rotate";
        public static string Rotate => ROTATE;
        //
    }
}
