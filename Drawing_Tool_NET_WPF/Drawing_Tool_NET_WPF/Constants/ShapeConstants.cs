﻿
namespace Drawing_Tool_NET_WPF.Constants
{
    public class ShapeConstants
    {
        // created the props because data-binding requires it !!
        public const string SELECTOR = "selector";
        public static string Selector => SELECTOR;

        public const string ELLIPSE = "ellipse";
        public static string Ellipse => ELLIPSE;

        public const string LINE = "line";
        public static string Line => LINE;

        public const string RECTANGLE = "rectangle";
        public static string Rectangle => RECTANGLE;

        public const string RECTANGLE_DIAGONAL = "rectangle_diagonal";
        public static string RectangleDiagonal => RECTANGLE_DIAGONAL;
        //
        public const float SCALE_COEF = 1;

        public const string UNSUPPORTED_SHAPE = "ShapeConverter: Unsupported shape with tag: ";
    }
}
