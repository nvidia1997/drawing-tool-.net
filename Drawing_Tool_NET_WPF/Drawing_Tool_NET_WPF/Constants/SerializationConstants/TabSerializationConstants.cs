﻿
namespace Drawing_Tool_NET_WPF.Constants.SerializationConstants
{
    public class TabSerializationConstants // constants used for de/serialisation of props
    {
        public const string HEADER = "Header";
        public const string SHAPES = "Shapes";
    }
}
