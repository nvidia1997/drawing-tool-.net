﻿
namespace Drawing_Tool_NET_WPF.Constants.SerializationConstants
{
    public class ShapeSerializationConstants // constants used for de/serialisation of props
    {
        public const string TAG = "Tag";
        public const string START_POINT = "StartPoint";
        public const string END_POINT = "EndPoint";
        public const string ROTATION_ANGLE = "RotationAngle";
        public const string SCALE_COEF = "ScaleCoef";
        public const string TRANSLATION = "Translation";
        public const string STROKE_THICKNESS = "StrokeThickness";
        public const string FILL = "Fill";
        public const string STROKE = "Stroke";
    }
}
